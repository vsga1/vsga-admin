import { Icon } from '@chakra-ui/react';
import { MdPerson, MdHome, MdFeaturedVideo, MdLabel } from 'react-icons/md';
import SignIn from './pages/auth/signIn';
import Room from './pages/room';
import HomePage from './pages/home';
import User from './pages/user';
import Topic from './pages/topic';

const routes = [
  {
    name: 'Home',
    layout: 'default',
    path: '/home',
    icon: <Icon as={MdHome} width="20px" height="20px" color="inherit" />,
    component: <HomePage />,
  },
  {
    name: 'Study Room',
    layout: 'default',
    path: '/room',
    icon: (
      <Icon as={MdFeaturedVideo} width="20px" height="20px" color="inherit" />
    ),
    component: <Room />,
  },
  {
    name: 'User',
    layout: 'default',
    path: '/user',
    icon: <Icon as={MdPerson} width="20px" height="20px" color="inherit" />,
    component: <User />,
  },
  {
    name: 'Topic',
    layout: 'default',
    path: '/topic',
    icon: <Icon as={MdLabel} width="20px" height="20px" color="inherit" />,
    component: <Topic />,
  },
  {
    name: 'Sign In',
    layout: 'auth',
    path: '/sign-in',
    icon: <></>,
    component: <SignIn />,
  },
];

export default routes;

import { useCallback, useContext, useEffect, useState } from 'react';
import { Box, Button, ButtonGroup, Flex, useToast } from '@chakra-ui/react';
import TopicTable from '@/components/table/TopicTable';
import usePagination from '@/hooks/usePagination';
import { TopicResponse } from '@/utils/interface';
import Loading from '@/components/Loading';
import useAuthAxios from '@/hooks/useAuthAxios';
import { MdNavigateBefore, MdNavigateNext } from 'react-icons/md';
import { SearchBoxContext } from '@/contexts/SearchBoxContext';

interface SearchParams {
  limit: number;
  offset?: number;
  topic?: string;
}

export default function TopicPage() {
  const [list, setList] = useState<TopicResponse[]>([]);
  const [pagination, setPagination] = useState({
    total: 1,
    current: 1,
    perPage: 10,
  });
  const [loading, setLoading] = useState<boolean>(true);
  const paginateItems = usePagination(pagination.total, pagination.current);
  const { getAPI } = useAuthAxios();
  const toast = useToast();
  const { searchText } = useContext(SearchBoxContext);

  const searchTopicCallback = useCallback(
    async (offset = 0, searchText?: string) => {
      setLoading(true);
      const params: SearchParams = {
        limit: pagination.perPage,
      };
      if (searchText) {
        params.topic = searchText;
      }
      params.offset = offset;
      const { code, data, meta } = await getAPI('/topic/search', params);
      if (code === 200) {
        const total = Math.ceil(meta.total / meta.limit);
        const current = Math.floor(meta.offset / meta.limit) + 1;
        setPagination((prev) => ({
          ...prev,
          total,
          current,
        }));
        window.scrollTo(0, 0);
        setList(data);
      } else {
        toast({
          description: data as string,
          status: 'error',
          position: 'top-right',
        });
      }
      setLoading(false);
    },
    [getAPI, pagination.perPage, toast]
  );

  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      searchTopicCallback(0, searchText.trim());
    }, 500);
    return () => clearTimeout(delayDebounceFn);
  }, [searchTopicCallback, searchText]);

  return (
    <Box pt={{ base: '130px', md: '80px', xl: '80px' }}>
      {loading ? (
        <Flex justifyContent="center">
          <Loading />
        </Flex>
      ) : (
        <>
          <TopicTable data={list} setData={setList} />
          <ButtonGroup
            mt={5}
            width="100%"
            justifyContent="center"
            isAttached
            variant="outline"
            borderRadius="10px"
          >
            <Button
              p={2}
              isDisabled={pagination.current === 1}
              onClick={(e) => {
                e.currentTarget.blur();
                if (pagination.current > 1) {
                  searchTopicCallback(
                    (pagination.current - 2) * pagination.perPage,
                    searchText.trim()
                  );
                }
              }}
            >
              <MdNavigateBefore />
            </Button>
            {paginateItems.map((item, index) => {
              const cur = pagination.current;
              return (
                <Button
                  key={index}
                  variant={item === cur ? 'primary' : 'outline'}
                  isDisabled={item === '...'}
                  onClick={(e) => {
                    e.currentTarget.blur();
                    if (item !== cur && typeof item === 'number') {
                      searchTopicCallback(
                        (item - 1) * pagination.perPage,
                        searchText.trim()
                      );
                    }
                  }}
                >
                  {item}
                </Button>
              );
            })}
            <Button
              p={2}
              isDisabled={pagination.current === pagination.total}
              onClick={(e) => {
                e.currentTarget.blur();
                if (pagination.current < pagination.total) {
                  searchTopicCallback(
                    pagination.current * pagination.perPage,
                    searchText.trim()
                  );
                }
              }}
            >
              <MdNavigateNext />
            </Button>
          </ButtonGroup>
        </>
      )}
    </Box>
  );
}

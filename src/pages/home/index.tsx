import Statistics from '@/components/card/Statistics';
import IconBox from '@/components/icon/IconBox';
import {
  Box,
  Flex,
  Icon,
  SimpleGrid,
  useColorModeValue,
  useToast,
} from '@chakra-ui/react';
import { FaUserAlt, FaUserCheck } from 'react-icons/fa';
import { MdMeetingRoom } from 'react-icons/md';
import ChartCard from '@/components/card/Chart';
import LineChart from '@/components/chart/LineChart';
import PieChart from '@/components/chart/PieChart';
import { useEffect, useState } from 'react';
import useAuthAxios from '@/hooks/useAuthAxios';
import Loading from '@/components/Loading';

interface SummaryState {
  users: number;
  rooms: number;
  newUserPerMonth: number[];
  monthLabel: string[];
  topics: number[];
  topicsLabel: string[];
}

const HomePage = () => {
  const brandColor = useColorModeValue('vstudy.primary', 'white');
  const boxBg = useColorModeValue('secondaryGray.300', 'whiteAlpha.100');
  const [summary, setSummary] = useState<SummaryState>({
    users: 0,
    rooms: 0,
    newUserPerMonth: [],
    monthLabel: [],
    topics: [],
    topicsLabel: [],
  });
  const [loading, setLoading] = useState<boolean>(true);
  const { getAPI } = useAuthAxios();
  const toast = useToast();

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      const monthSummary = 6;
      const res = await Promise.all([
        getAPI('/room/summary'),
        getAPI(`/user/summary?months=${monthSummary}`),
      ]);
      if (res[0].code === 200 && res[1].code === 200) {
        const { data: roomData } = res[0];
        const { data: userData } = res[1];
        const newSum: SummaryState = {
          users: userData.total_active_user,
          rooms: roomData.total_active_room,
          newUserPerMonth: [],
          monthLabel: [],
          topics: [],
          topicsLabel: [],
        };
        roomData.topic_summary.forEach(
          (item: { topic: string; quantity: number }) => {
            newSum.topics.push(item.quantity);
            newSum.topicsLabel.push(item.topic);
          }
        );
        userData.new_users_per_month.forEach(
          (item: { month: string; total: number }) => {
            newSum.newUserPerMonth.push(item.total);
            newSum.monthLabel.push(item.month.substring(0, 3));
          }
        );
        setSummary(newSum);
        setLoading(false);
      } else {
        toast({
          description: res[0].data,
          status: 'error',
          position: 'top-right',
        });
        toast({
          description: res[1].data,
          status: 'error',
          position: 'top-right',
        });
      }
    };
    fetchData();
  }, [getAPI, toast]);

  return (
    <Box pt={{ base: '130px', md: '80px', xl: '80px' }}>
      {loading ? (
        <Flex justifyContent="center">
          <Loading />
        </Flex>
      ) : (
        <>
          <SimpleGrid
            columns={{ base: 1, md: 2, lg: 3, '2xl': 6 }}
            gap="20px"
            mb="20px"
          >
            <Statistics
              startContent={
                <IconBox
                  w="56px"
                  h="56px"
                  bg={boxBg}
                  icon={
                    <Icon w="20px" h="20px" as={FaUserAlt} color={brandColor} />
                  }
                />
              }
              name="Users"
              value={summary.users}
            />
            <Statistics
              startContent={
                <IconBox
                  w="56px"
                  h="56px"
                  bg={boxBg}
                  icon={
                    <Icon
                      w="24px"
                      h="24px"
                      as={MdMeetingRoom}
                      color={brandColor}
                    />
                  }
                />
              }
              name="Rooms"
              value={summary.rooms}
            />
            <Statistics
              startContent={
                <IconBox
                  w="56px"
                  h="56px"
                  bg={boxBg}
                  icon={
                    <Icon
                      w="24px"
                      h="24px"
                      as={FaUserCheck}
                      color={brandColor}
                    />
                  }
                />
              }
              name="New user this month"
              value={
                summary.newUserPerMonth[summary.newUserPerMonth.length - 1]
              }
              growth=""
            />
          </SimpleGrid>
          <SimpleGrid columns={{ base: 1, md: 2, xl: 2 }} gap="20px" mb="20px">
            <ChartCard title="Monthly Traffic">
              <LineChart
                chartData={[
                  {
                    name: 'New Users',
                    data: summary.newUserPerMonth,
                  },
                ]}
                categories={summary.monthLabel}
              />
            </ChartCard>
            <ChartCard title="Popular Topic" dataLabels={summary.topicsLabel}>
              <PieChart
                chartData={summary.topics}
                chartLabel={summary.topicsLabel}
              />
            </ChartCard>
          </SimpleGrid>
        </>
      )}
    </Box>
  );
};

export default HomePage;

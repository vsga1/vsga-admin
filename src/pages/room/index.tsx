import { useCallback, useContext, useEffect, useState } from 'react';
import { Box, Button, ButtonGroup, Flex, useToast } from '@chakra-ui/react';
import RoomTable from '@/components/table/RoomTable';
import usePagination from '@/hooks/usePagination';
import { Room } from '@/utils/interface';
import Loading from '@/components/Loading';
import useAuthAxios from '@/hooks/useAuthAxios';
import { MdNavigateBefore, MdNavigateNext } from 'react-icons/md';
import { SearchBoxContext } from '@/contexts/SearchBoxContext';

interface SearchParams {
  limit: number;
  offset?: number;
  name?: string;
  topics?: string;
}

export default function RoomPage() {
  const [list, setList] = useState<Room[]>([]);
  const [pagination, setPagination] = useState({
    total: 1,
    current: 1,
    perPage: 10,
  });
  const [loading, setLoading] = useState<boolean>(true);
  const paginateItems = usePagination(pagination.total, pagination.current);
  const { getAPI } = useAuthAxios();
  const toast = useToast();
  const { searchText } = useContext(SearchBoxContext);

  const searchRoomCallback = useCallback(
    async (offset = 0, searchText?: string, topics?: string[]) => {
      setLoading(true);
      const params: SearchParams = {
        limit: pagination.perPage,
      };
      if (searchText) {
        params.name = searchText;
      }
      if (offset) {
        params.offset = offset;
      }
      if (topics?.length) {
        params.topics = topics.join(',');
      }
      const { code, data, meta } = await getAPI(
        '/room/search',
        params
      );
      if (code === 200) {
        const total = Math.ceil(meta.total / meta.limit);
        const current = Math.floor(meta.offset / meta.limit) + 1;
        setPagination((prev) => ({
          ...prev,
          total,
          current,
        }));
        window.scrollTo(0, 0);
        setList(data);
      } else {
        toast({
          description: data as string,
          status: 'error',
          position: 'top-right',
        });
      }
      setLoading(false);
    },
    [getAPI, pagination.perPage, toast]
  );

  // useEffect(() => {
  //   searchRoomCallback();
  // }, [searchRoomCallback]);

  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      searchRoomCallback(0, searchText.trim());
    }, 500);
    return () => clearTimeout(delayDebounceFn);
  }, [searchRoomCallback, searchText]);

  return (
    <Box pt={{ base: '130px', md: '80px', xl: '80px' }}>
      {loading ? (
        <Flex justifyContent="center">
          <Loading />
        </Flex>
      ) : (
        <>
          <RoomTable data={list} setData={setList} />
          <ButtonGroup
            mt={5}
            width="100%"
            justifyContent="center"
            isAttached
            variant="outline"
            borderRadius="10px"
          >
            <Button
              p={2}
              isDisabled={pagination.current === 1}
              onClick={(e) => {
                e.currentTarget.blur();
                if (pagination.current > 1) {
                  searchRoomCallback(
                    (pagination.current - 2) * pagination.perPage,
                    searchText.trim()
                  );
                }
              }}
            >
              <MdNavigateBefore />
            </Button>
            {paginateItems.map((item, index) => {
              const cur = pagination.current;
              return (
                <Button
                  key={index}
                  variant={item === cur ? 'primary' : 'outline'}
                  isDisabled={item === '...'}
                  onClick={(e) => {
                    e.currentTarget.blur();
                    if (item !== cur && typeof item === 'number') {
                      searchRoomCallback(
                        (item - 1) * pagination.perPage,
                        searchText.trim()
                      );
                    }
                  }}
                >
                  {item}
                </Button>
              );
            })}
            <Button
              p={2}
              isDisabled={pagination.current === pagination.total}
              onClick={(e) => {
                e.currentTarget.blur();
                if (pagination.current < pagination.total) {
                  searchRoomCallback(
                    pagination.current * pagination.perPage,
                    searchText.trim()
                  );
                }
              }}
            >
              <MdNavigateNext />
            </Button>
          </ButtonGroup>
        </>
      )}
    </Box>
  );
}

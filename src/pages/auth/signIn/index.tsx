import { useState } from 'react';
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  Icon,
  Input,
  InputGroup,
  InputRightElement,
  Text,
  useColorModeValue,
  useToast,
} from '@chakra-ui/react';
import { yupResolver } from '@hookform/resolvers/yup';
import schema from '@/utils/yupSchema/signInForm';
import DefaultAuth from '@/layouts/auth/Default';
import { MdOutlineRemoveRedEye } from 'react-icons/md';
import { RiEyeCloseLine } from 'react-icons/ri';
import { SubmitHandler, useForm } from 'react-hook-form';
import axios from 'axios';
import { SERVER_ERROR } from '@/utils/constants/message';
import { useSignIn } from 'react-auth-kit';
import { useNavigate } from 'react-router-dom';
import { TOKEN_EXPIRE_IN } from '@/utils/constants/number';

interface FormData {
  username: string;
  password: string;
}

function SignIn() {
  const toast = useToast();
  const signIn = useSignIn();
  const navigate = useNavigate();
  const textColor = useColorModeValue('navy.700', 'white');
  const textColorSecondary = 'gray.400';
  const [show, setShow] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleClick = () => setShow(!show);

  const {
    register,
    handleSubmit,
    clearErrors,
    formState: { errors },
  } = useForm<FormData>({
    resolver: yupResolver(schema),
    mode: 'onSubmit',
    reValidateMode: 'onSubmit',
  });

  const onSubmit: SubmitHandler<FormData> = async (data) => {
    setLoading(true);
    const { username, password } = data;
    try {
      const {
        data: { data, meta },
      } = await axios.post(
        `${import.meta.env.VITE_API_URL}/v1/account/admin/login`,
        {
          username,
          password,
        }
      );
      if (meta.code !== 200) {
        toast({
          description: meta?.message,
          status: 'error',
          duration: 5000,
          isClosable: true,
          position: 'top-right',
        });
      } else {
        if (
          signIn({
            token: data.access_token,
            expiresIn: TOKEN_EXPIRE_IN,
            tokenType: 'Bearer',
            authState: { username },
          })
        ) {
          navigate('/home');
        } else {
          toast({
            description: SERVER_ERROR,
            status: 'error',
            duration: 5000,
            isClosable: true,
            position: 'top-right',
          });
        }
      }
    } catch (error) {
      let msg = SERVER_ERROR;
      if (axios.isAxiosError(error) && error?.response?.data?.meta?.message) {
        msg = error.response.data.meta.message;
      }
      toast({
        description: msg,
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }

    setLoading(false);
  };

  return (
    <DefaultAuth>
      <Flex
        maxW={{ base: '100%', md: 'max-content' }}
        w="100%"
        mx={{ base: 'auto', lg: '0px' }}
        me="auto"
        h="100%"
        alignItems="start"
        justifyContent="center"
        mb={{ base: '30px', md: '60px' }}
        px={{ base: '25px', md: '0px' }}
        mt={{ base: '40px', md: '14vh' }}
        flexDirection="column"
      >
        <Box mx="auto">
          <Heading
            color={textColor}
            fontSize="36px"
            mb="10px"
            textAlign="center"
          >
            Sign In
          </Heading>
          <Text
            mb="36px"
            ms="4px"
            color={textColorSecondary}
            fontWeight="400"
            fontSize="md"
          >
            Sign In to Dashboard!
          </Text>
        </Box>
        <Flex
          zIndex="2"
          direction="column"
          w={{ base: '100%', md: '420px' }}
          background="transparent"
          mx={{ base: 'auto', lg: 'unset' }}
          mb={{ base: '20px', md: 'auto' }}
          as="form"
          onSubmit={handleSubmit(onSubmit)}
        >
          <FormControl mb="24px" isInvalid={!!errors.username}>
            <FormLabel
              display="flex"
              ms="4px"
              fontSize="sm"
              fontWeight="500"
              color={textColor}
              mb="8px"
            >
              Username
            </FormLabel>
            <Input
              variant="auth"
              fontSize="sm"
              ms={{ base: '0px', md: '0px' }}
              type="text"
              placeholder="Username"
              fontWeight="500"
              size="lg"
              {...register('username', {
                onChange: () => {
                  clearErrors('username');
                },
              })}
            />
            <FormErrorMessage>{errors.username?.message}</FormErrorMessage>
          </FormControl>
          <FormControl mb="24px" isInvalid={!!errors.password}>
            <FormLabel
              ms="4px"
              fontSize="sm"
              fontWeight="500"
              color={textColor}
              display="flex"
            >
              Password
            </FormLabel>
            <InputGroup size="md">
              <Input
                fontSize="sm"
                placeholder="Password"
                size="lg"
                type={show ? 'text' : 'password'}
                variant="auth"
                {...register('password', {
                  onChange: () => {
                    clearErrors('password');
                  },
                })}
              />
              <InputRightElement display="flex" alignItems="center" mt="4px">
                <Icon
                  color={textColorSecondary}
                  _hover={{ cursor: 'pointer' }}
                  as={show ? RiEyeCloseLine : MdOutlineRemoveRedEye}
                  onClick={handleClick}
                />
              </InputRightElement>
            </InputGroup>
            <FormErrorMessage>{errors.password?.message}</FormErrorMessage>
          </FormControl>
          <Button
            fontSize="sm"
            variant="primary"
            h="50"
            type="submit"
            isLoading={loading}
            loadingText="Submitting..."
          >
            Sign In
          </Button>
        </Flex>
      </Flex>
    </DefaultAuth>
  );
}

export default SignIn;

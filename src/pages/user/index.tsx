import { useCallback, useContext, useEffect, useState } from 'react';
import { Box, Button, ButtonGroup, Flex, useToast } from '@chakra-ui/react';
import UserTable from '@/components/table/UserTable';
import usePagination from '@/hooks/usePagination';
import { User } from '@/utils/interface';
import Loading from '@/components/Loading';
import useAuthAxios from '@/hooks/useAuthAxios';
import { MdNavigateBefore, MdNavigateNext } from 'react-icons/md';
import { SearchBoxContext } from '@/contexts/SearchBoxContext';

interface SearchParams {
  limit: number;
  offset?: number;
  username?: string;
  topics?: string;
}

export default function UserPage() {
  const [list, setList] = useState<User[]>([]);
  const [pagination, setPagination] = useState({
    total: 1,
    current: 1,
    perPage: 10,
  });
  const [loading, setLoading] = useState<boolean>(true);
  const paginateItems = usePagination(pagination.total, pagination.current);
  const { getAPI } = useAuthAxios();
  const toast = useToast();
  const { searchText } = useContext(SearchBoxContext);

  const searchUserCallback = useCallback(
    async (offset = 0, searchText?: string, topics?: string[]) => {
      setLoading(true);
      const params: SearchParams = {
        limit: pagination.perPage,
      };
      if (searchText) {
        params.username = searchText;
      }
      params.offset = offset;
      if (topics?.length) {
        params.topics = topics.join(',');
      }
      const { code, data, meta } = await getAPI(
        '/user/search',
        params
      );
      if (code === 200) {
        const total = Math.ceil(meta.total / meta.limit);
        const current = Math.floor(meta.offset / meta.limit) + 1;
        setPagination((prev) => ({
          ...prev,
          total,
          current,
        }));
        window.scrollTo(0, 0);
        setList(data);
      } else {
        toast({
          description: data as string,
          status: 'error',
          position: 'top-right',
        });
      }
      setLoading(false);
    },
    [getAPI, pagination.perPage, toast]
  );

  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      searchUserCallback(0, searchText.trim());
    }, 500);
    return () => clearTimeout(delayDebounceFn);
  }, [searchUserCallback, searchText]);

  return (
    <Box pt={{ base: '130px', md: '80px', xl: '80px' }}>
      {loading ? (
        <Flex justifyContent="center">
          <Loading />
        </Flex>
      ) : (
        <>
          <UserTable data={list} setData={setList} />
          <ButtonGroup
            mt={5}
            width="100%"
            justifyContent="center"
            isAttached
            variant="outline"
            borderRadius="10px"
          >
            <Button
              p={2}
              isDisabled={pagination.current === 1}
              onClick={(e) => {
                e.currentTarget.blur();
                if (pagination.current > 1) {
                  searchUserCallback(
                    (pagination.current - 2) * pagination.perPage,
                    searchText.trim()
                  );
                }
              }}
            >
              <MdNavigateBefore />
            </Button>
            {paginateItems.map((item, index) => {
              const cur = pagination.current;
              return (
                <Button
                  key={index}
                  variant={item === cur ? 'primary' : 'outline'}
                  isDisabled={item === '...'}
                  onClick={(e) => {
                    e.currentTarget.blur();
                    if (item !== cur && typeof item === 'number') {
                      searchUserCallback(
                        (item - 1) * pagination.perPage,
                        searchText.trim()
                      );
                    }
                  }}
                >
                  {item}
                </Button>
              );
            })}
            <Button
              p={2}
              isDisabled={pagination.current === pagination.total}
              onClick={(e) => {
                e.currentTarget.blur();
                if (pagination.current < pagination.total) {
                  searchUserCallback(
                    pagination.current * pagination.perPage,
                    searchText.trim()
                  );
                }
              }}
            >
              <MdNavigateNext />
            </Button>
          </ButtonGroup>
        </>
      )}
    </Box>
  );
}

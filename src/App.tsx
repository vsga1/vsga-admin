import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { AuthProvider, RequireAuth } from 'react-auth-kit';
import AdminLayout from '@/layouts/admin';
import AuthLayout from '@/layouts/auth';

function App() {
  return (
    <AuthProvider authType="localstorage" authName="_auth">
      <BrowserRouter>
        <Routes>
          <Route path="/auth/*" element={<AuthLayout />} />
          <Route
            path="*"
            element={
              <RequireAuth loginPath="/auth/sign-in">
                <AdminLayout />
              </RequireAuth>
            }
          />
        </Routes>
      </BrowserRouter>
    </AuthProvider>
  );
}

export default App;

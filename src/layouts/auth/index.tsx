import { useState } from 'react';
import { Route, Routes } from 'react-router-dom';
import routes from '@/routes';

import { Box, useColorModeValue } from '@chakra-ui/react';

import { SidebarContext } from '@/contexts/SidebarContext';

export default function Auth() {
  const [toggleSidebar, setToggleSidebar] = useState(false);

  const getRoutes = (routes: RoutesType[]) => {
    return routes.map((route, key) => {
      if (route.layout === 'auth') {
        return <Route path={route.path} element={route.component} key={key} />;
      }
    });
  };
  const authBg = useColorModeValue('white', 'navy.900');

  return (
    <Box>
      <SidebarContext.Provider
        value={{
          toggleSidebar,
          setToggleSidebar,
        }}
      >
        <Box
          bg={authBg}
          float="right"
          minHeight="100vh"
          height="100%"
          position="relative"
          w="100%"
          transition="all 0.33s cubic-bezier(0.685, 0.0473, 0.346, 1)"
          transitionDuration=".2s, .2s, .35s"
          transitionProperty="top, bottom, width"
          transitionTimingFunction="linear, linear, ease"
          overflow="hidden"
        >
          <Box mx="auto" minH="100vh">
            <Routes>{getRoutes(routes)}</Routes>
          </Box>
        </Box>
      </SidebarContext.Provider>
    </Box>
  );
}

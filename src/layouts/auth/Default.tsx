import { Flex } from '@chakra-ui/react';
import { ReactElement } from 'react';

function AuthIllustration(props: { children: ReactElement | string }) {
  const { children } = props;
  return (
    <Flex
      h={{
        sm: 'initial',
        md: 'unset',
        lg: '100vh',
        xl: '97vh',
      }}
      w="100%"
      maxW={{ md: '66%', lg: '1313px' }}
      mx="auto"
      pt={{ sm: '50px', md: '0px' }}
      px={{ lg: '30px', xl: '0px' }}
      ps={{ xl: '70px' }}
      justifyContent="center"
    >
      {children}
    </Flex>
  );
}

export default AuthIllustration;

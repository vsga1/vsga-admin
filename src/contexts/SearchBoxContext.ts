import React, { createContext } from 'react';
interface SearchBoxContext {
  searchText: string;
  setSearchText: React.Dispatch<React.SetStateAction<string>>;
}

export const SearchBoxContext = createContext({} as SearchBoxContext);

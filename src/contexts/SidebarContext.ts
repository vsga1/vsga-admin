import React,{ createContext } from 'react';
interface sidebarContext {
  toggleSidebar: boolean;
  setToggleSidebar: React.Dispatch<React.SetStateAction<boolean>>;
}

export const SidebarContext = createContext<sidebarContext | null>(null);

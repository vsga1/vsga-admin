export const toLocalDateTime = (datetime: string) => {
  if (!datetime) return '';
  return new Date(`${datetime}Z`).toLocaleString('en-GB');
}

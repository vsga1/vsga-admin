export interface Room {
  code: string;
  name: string;
  room_type: 'PUBLIC' | 'PRIVATE';
  description: string | null;
  topics: Topic[];
  created_at: string;
  created_by: string;
  status: 'ACTIVE' | 'INACTIVE';
}
export interface User {
  code: string;
  username: string;
  email: string;
  avatar_img_url: string;
  created_at: string;
  status: 'ACTIVE' | 'INACTIVE';
}
export interface Topic {
  name: string;
  color: string;
}
export interface TopicResponse extends Topic {
  created_at: string;
  created_by: string;
}

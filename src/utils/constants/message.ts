export const SERVER_ERROR =
  'There was a problem with server. Please try again later.';
export const CONFIRM = 'Are you sure you want to do this action?';

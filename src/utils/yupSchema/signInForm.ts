import * as yup from 'yup';

const schema = yup
  .object({
    username: yup.string().required('Please enter your username.'),
    password: yup.string().required('Please enter your password.'),
  })
  .required();

export default schema;

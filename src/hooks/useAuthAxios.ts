import { SERVER_ERROR } from '@/utils/constants/message';
import axios, { AxiosHeaders } from 'axios';
import { useCallback } from 'react';
import { useAuthHeader } from 'react-auth-kit';

function useAuthAxios() {
  const authHeader = useAuthHeader();

  axios.interceptors.request.use(async (config) => {
    config.baseURL = `${import.meta.env.VITE_API_URL}/v1/admin`;
    (config.headers as AxiosHeaders).set('Authorization', authHeader());
    (config.headers as AxiosHeaders).set('Content-Type', 'application/json');
    return config;
  });

  const getAPI = useCallback(async (url: string, params?: unknown) => {
    return await axios
      .get(url, { params })
      .then((res) => res.data)
      .then((res) => {
        const { meta, data } = res;
        const resData = data || meta.message;
        return {
          meta,
          code: meta.code as number,
          data: resData,
        };
      })
      .catch((error) => {
        return {
          meta: error.response?.data?.meta,
          code: error.response?.data?.meta?.code as number,
          data: error.response?.data?.meta?.message || SERVER_ERROR,
        };
      });
  }, []);

  const postAPI = useCallback(async (url: string, data: unknown) => {
    return await axios
      .post(url, data)
      .then((res) => res.data)
      .then((res) => {
        const { meta, data } = res;
        const resData = data || meta.message;
        return {
          meta,
          code: meta.code as number,
          data: resData,
        };
      })
      .catch((error) => {
        return {
          meta: error.response?.data?.meta,
          code: error.response?.data?.meta?.code as number,
          data: error.response?.data?.meta?.message || SERVER_ERROR,
        };
      });
  }, []);

  const putAPI = useCallback(async (url: string, data: unknown) => {
    return await axios
      .put(url, data)
      .then((res) => res.data)
      .then((res) => {
        const { meta, data } = res;
        const resData = data || meta.message;
        return {
          meta,
          code: meta.code as number,
          data: resData,
        };
      })
      .catch((error) => {
        return {
          meta: error.response?.data?.meta,
          code: error.response?.data?.meta?.code as number,
          data: error.response?.data?.meta?.message || SERVER_ERROR,
        };
      });
  }, []);

  const deleteAPI = useCallback(async (url: string) => {
    return await axios
      .delete(url)
      .then((res) => res.data)
      .then((res) => {
        const { meta, data } = res;
        const resData = data || meta.message;
        return {
          code: meta.code as number,
          data: resData,
        };
      })
      .catch((error) => {
        return {
          code: error?.response?.data?.meta?.code as number,
          data: error?.response?.data?.meta?.message || SERVER_ERROR,
        };
      });
  }, []);

  return {
    getAPI,
    postAPI,
    putAPI,
    deleteAPI,
  };
}
export default useAuthAxios;

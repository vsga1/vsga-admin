import { Flex, useColorModeValue } from '@chakra-ui/react';
import Logo from '@/assets/logo.png';
import { HSeparator } from '@/components/separator/Separator';

export function SidebarBrand() {
  const logoColor = useColorModeValue('vstudy.primary', 'unset');

  return (
    <Flex alignItems="center" flexDirection="column">
      <Flex
        alignItems="center"
        mb="12px"
        gap="10px"
        color={logoColor}
        fontSize="20px"
        fontWeight="700"
      >
        <img src={Logo} alt="" width={40} />
        VStudy
      </Flex>

      <HSeparator mb="20px" />
    </Flex>
  );
}

export default SidebarBrand;

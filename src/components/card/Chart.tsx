import { Box, Flex, Grid, Text, useColorModeValue } from '@chakra-ui/react';
import Card from '@/components/card/Card';
import { ReactElement } from 'react';

export default function WeeklyRevenue(props: {
  title: string;
  children: ReactElement;
  dataLabels?: string[];
  [x: string]: unknown;
}) {
  const { title, dataLabels, children, ...rest } = props;
  const colorChartToken = [
    'vstudy.primary',
    'vstudy.secondary',
    'vstudy.secondary-shades',
    'vstudy.tertiary',
    'vstudy.tertiary-shades',
  ];
  const textColor = useColorModeValue('secondaryGray.900', 'white');
  const cardColor = useColorModeValue('white', 'navy.700');
  const cardShadow = useColorModeValue(
    '0px 0px 4px rgba(112, 144, 176, 0.3)',
    'unset'
  );

  return (
    <Card alignItems="center" flexDirection="column" w="100%" {...rest}>
      <Flex align="center" w="100%" px="15px" py="10px">
        <Text
          me="auto"
          color={textColor}
          fontSize="xl"
          fontWeight="700"
          lineHeight="100%"
        >
          {title}
        </Text>
      </Flex>

      <Box min-height="240px" my="auto" w="100%">
        {dataLabels?.length ? (
          <Grid
            templateColumns={{ base: '1fr', md: '7fr 3fr', xl: '7fr 3fr' }}
            gap={6}
          >
            <Box w="100%">{children}</Box>
            <Card
              bg={cardColor}
              flexDirection="row"
              boxShadow={cardShadow}
              w="100%"
              px="20px"
              mt="15px"
              mx="auto"
            >
              <Flex flexDirection="column" gap={3} py="5px">
                {dataLabels.map((item, index) => (
                  <Flex alignItems="center">
                    <Box
                      h="8px"
                      w="8px"
                      bg={colorChartToken[index]}
                      borderRadius="50%"
                      me="4px"
                    />
                    <Text
                      fontSize="xs"
                      color="secondaryGray.600"
                      fontWeight="700"
                    >
                      {item}
                    </Text>
                  </Flex>
                ))}
              </Flex>
            </Card>
          </Grid>
        ) : (
          children
        )}
      </Box>
    </Card>
  );
}

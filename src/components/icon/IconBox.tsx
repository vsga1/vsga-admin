import { Flex } from '@chakra-ui/react';
import { ReactElement } from 'react';

export default function IconBox(props: {
  icon: ReactElement | string;
  [x: string]: unknown;
}) {
  const { icon, ...rest } = props;

  return (
    <Flex
      alignItems={'center'}
      justifyContent={'center'}
      borderRadius={'50%'}
      {...rest}
    >
      {icon}
    </Flex>
  );
}

import { useToken } from '@chakra-ui/react';
import ReactApexChart from 'react-apexcharts';

type StrokeCurveType = 'smooth' | 'straight' | 'stepline';
type xAxisType = 'numeric' | 'datetime' | 'category';
type ChartData = {
  name: string;
  data: number[];
};

const LineChart = (props: { chartData: ChartData[], categories?: string[] }) => {
  const { chartData, categories } = props;
  const colorChart = useToken('colors', ['vstudy.primary', 'vstudy.tertiary']);
  const lineChartOption = {
    chart: {
      toolbar: {
        show: false,
      },
      dropShadow: {
        enabled: true,
        top: 13,
        left: 0,
        blur: 10,
        opacity: 0.1,
        color: '#4318FF',
      },
    },
    colors: colorChart,
    markers: {
      size: 0,
      colors: 'white',
      strokeColors: colorChart[0],
      strokeWidth: 3,
      strokeOpacity: 0.9,
      strokeDashArray: 0,
      fillOpacity: 1,
      radius: 2,
      offsetX: 0,
      offsetY: 0,
      showNullDataPoints: true,
    },
    tooltip: {
      theme: 'dark',
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      curve: 'smooth' as StrokeCurveType,
      type: 'line',
    },
    xaxis: {
      type: 'category' as xAxisType,
      categories: categories?.length ? categories : ['SEP', 'OCT', 'NOV', 'DEC', 'JAN', 'FEB'],
      labels: {
        style: {
          colors: '#A3AED0',
          fontSize: '12px',
          fontWeight: '500',
        },
      },
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false,
      },
    },
    yaxis: {
      show: false,
    },
    legend: {
      show: false,
    },
    grid: {
      show: false,
      column: {
        color: ['#7551FF', '#39B8FF'],
        opacity: 0.5,
      },
    },
  };
  return (
    <ReactApexChart
      options={lineChartOption}
      series={chartData}
      type="line"
    />
  );
};

export default LineChart;

/* eslint-disable no-undef */
import { useToken } from '@chakra-ui/react';
import ReactApexChart from 'react-apexcharts';

const PieChart = (props: { chartData: number[]; chartLabel: string[] }) => {
  const { chartData, chartLabel } = props;
  const colorChart = useToken('colors', [
    'vstudy.primary',
    'vstudy.secondary',
    'vstudy.secondary-shades',
    'vstudy.tertiary',
    'vstudy.tertiary-shades',
  ]);
  const pieChartOptions = {
    labels: chartLabel,
    colors: [colorChart],
    chart: {
      width: '50px',
    },
    states: {
      hover: {
        filter: {
          type: 'none',
        },
      },
    },
    stroke: {
      show: false,
    },
    legend: {
      show: false,
    },
    dataLabels: {
      enabled: false,
    },
    hover: { mode: null },
    plotOptions: {
      donut: {
        expandOnClick: false,
        donut: {
          labels: {
            show: false,
          },
        },
      },
    } as ApexPlotOptions,
    fill: {
      colors: [...colorChart],
    },
    tooltip: {
      enabled: true,
      theme: 'dark',
    },
  };
  return (
    <ReactApexChart options={pieChartOptions} series={chartData} type="pie" />
  );
};

export default PieChart;

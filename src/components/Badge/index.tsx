import { Badge, BadgeProps } from '@chakra-ui/react';
import chroma from 'chroma-js';
import React from 'react';

interface Props extends BadgeProps {
  color: string;
  children: React.ReactNode;
}

const MyBadge = ({ color, children, ...rest }: Props) => {
  return (
    <Badge
      bg={color}
      color={chroma.contrast(color, 'white') > 1.5 ? 'white' : 'black'}
      {...rest}
    >
      {children}
    </Badge>
  );
};

export default MyBadge;

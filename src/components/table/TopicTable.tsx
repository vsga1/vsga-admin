import {
  Flex,
  Box,
  Table,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useColorModeValue,
  Button,
  AlertDialog,
  AlertDialogOverlay,
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogBody,
  AlertDialogFooter,
  useDisclosure,
  useToast,
  Tooltip,
} from '@chakra-ui/react';
import { Dispatch, SetStateAction, useRef, useState } from 'react';
import {
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  getSortedRowModel,
  SortingState,
  useReactTable,
} from '@tanstack/react-table';
import { BsFillCaretUpFill, BsCaretDownFill } from 'react-icons/bs';
import { MdDelete } from 'react-icons/md';
import Card from '@/components/card/Card';
import { TopicResponse } from '@/utils/interface';
import useAuthAxios from '@/hooks/useAuthAxios';
import { CONFIRM } from '@/utils/constants/message';
import { toLocalDateTime } from '@/utils/datetime';
import MyBadge from '../Badge';

const columnHelper = createColumnHelper<TopicResponse>();

export default function CheckTable(props: {
  data: TopicResponse[];
  setData: Dispatch<SetStateAction<TopicResponse[]>>;
}) {
  const { data, setData } = props;
  const [sorting, setSorting] = useState<SortingState>([]);
  const textColor = useColorModeValue('secondaryGray.900', 'white');
  const borderColor = useColorModeValue('gray.200', 'whiteAlpha.100');
  const [selectedTopic, setSelectedTopic] = useState(-1);
  const { getAPI } = useAuthAxios();
  const toast = useToast();
  const deleteModal = useDisclosure();
  const lockRef = useRef(null);

  const columns = [
    columnHelper.accessor('name', {
      id: 'name',
      header: () => (
        <Text
          justifyContent="space-between"
          align="center"
          fontSize={{ sm: '10px', lg: '12px' }}
          color="gray.400"
        >
          NAME
        </Text>
      ),
      cell: (info) => (
        <Flex align="center">
          <Text color={textColor} fontSize="sm" fontWeight="700">
            {info.getValue()}
          </Text>
        </Flex>
      ),
    }),
    columnHelper.accessor('color', {
      id: 'color',
      header: () => (
        <Text
          justifyContent="space-between"
          align="center"
          fontSize={{ sm: '10px', lg: '12px' }}
          color="gray.400"
        >
          COLOR
        </Text>
      ),
      cell: (info) => (
        <MyBadge color={info.getValue()}>{info.getValue()}</MyBadge>
      ),
    }),
    columnHelper.accessor('created_at', {
      id: 'created_at',
      header: () => (
        <Text
          justifyContent="space-between"
          align="center"
          fontSize={{ sm: '10px', lg: '12px' }}
          color="gray.400"
        >
          CREATED AT
        </Text>
      ),
      cell: (info) => (
        <Text color={textColor} fontSize="sm" fontWeight="700">
          {toLocalDateTime(info.getValue())}
        </Text>
      ),
    }),
    columnHelper.accessor('created_by', {
      id: 'created_by',
      header: () => (
        <Text
          justifyContent="space-between"
          align="center"
          fontSize={{ sm: '10px', lg: '12px' }}
          color="gray.400"
        >
          CREATED BY
        </Text>
      ),
      cell: (info) => (
        <Text color={textColor} fontSize="sm" fontWeight="700">
          {info.getValue()}
        </Text>
      ),
    }),
    columnHelper.accessor('name', {
      id: 'delete',
      header: () => <></>,
      cell: (info) => (
        <Tooltip label="Delete">
          <Button
            colorScheme="red"
            w="40px"
            h="40px"
            p="0"
            onClick={() => {
              setSelectedTopic(info.row.index);
              deleteModal.onOpen();
            }}
          >
            <MdDelete />
          </Button>
        </Tooltip>
      ),
    }),
  ];

  const table = useReactTable({
    data,
    columns,
    state: {
      sorting,
    },
    onSortingChange: setSorting,
    getCoreRowModel: getCoreRowModel(),
    getSortedRowModel: getSortedRowModel(),
  });

  const handleCancel = () => {
    setSelectedTopic(-1);
    deleteModal.onClose();
  };

  const handleDelete = async () => {
    const res = await getAPI(`/topic/${data[selectedTopic].name}/delete`);
    if (res.code === 200) {
      toast({
        description: 'Topic is deleted.',
        status: 'success',
        position: 'top-right',
      });
      setData((prev) => {
        const res = [...prev];
        res.splice(selectedTopic, 1);
        return res;
      });
    } else {
      toast({
        description: res.data as string,
        status: 'error',
        position: 'top-right',
      });
    }
    setSelectedTopic(-1);
    deleteModal.onClose();
  };

  return (
    <Card flexDirection="column" w="100%" px="0px" overflowX="auto">
      <Box>
        <Table variant="simple" color="gray.500" mt="12px">
          <Thead>
            {table.getHeaderGroups().map((headerGroup) => (
              <Tr key={headerGroup.id}>
                {headerGroup.headers.map((header) => {
                  return (
                    <Th
                      key={header.id}
                      colSpan={header.colSpan}
                      pe="10px"
                      borderColor={borderColor}
                      cursor="pointer"
                      onClick={header.column.getToggleSortingHandler()}
                    >
                      <Flex
                        justifyContent="space-between"
                        align="center"
                        fontSize={{ sm: '10px', lg: '12px' }}
                        color="gray.400"
                      >
                        {flexRender(
                          header.column.columnDef.header,
                          header.getContext()
                        )}
                        {{
                          asc: <BsFillCaretUpFill />,
                          desc: <BsCaretDownFill />,
                        }[header.column.getIsSorted() as string] ?? null}
                      </Flex>
                    </Th>
                  );
                })}
              </Tr>
            ))}
          </Thead>
          <Tbody>
            {table
              .getRowModel()
              .rows.slice(0, 11)
              .map((row) => {
                return (
                  <Tr key={row.id}>
                    {row.getVisibleCells().map((cell) => {
                      return (
                        <Td
                          key={cell.id}
                          fontSize={{ sm: '14px' }}
                          minW={{ sm: '150px', md: '200px', lg: 'auto' }}
                          borderColor="transparent"
                        >
                          {flexRender(
                            cell.column.columnDef.cell,
                            cell.getContext()
                          )}
                        </Td>
                      );
                    })}
                  </Tr>
                );
              })}
          </Tbody>
        </Table>
      </Box>
      <AlertDialog
        isOpen={deleteModal.isOpen}
        leastDestructiveRef={lockRef}
        onClose={deleteModal.onClose}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Delete Topic
            </AlertDialogHeader>

            <AlertDialogBody>{CONFIRM}</AlertDialogBody>

            <AlertDialogFooter>
              <Button ref={lockRef} onClick={handleCancel}>
                Cancel
              </Button>
              <Button colorScheme="red" onClick={handleDelete} ml={3}>
                Delete
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </Card>
  );
}

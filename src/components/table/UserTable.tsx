import {
  Flex,
  Box,
  Table,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useColorModeValue,
  Button,
  AlertDialog,
  AlertDialogOverlay,
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogBody,
  AlertDialogFooter,
  useDisclosure,
  useToast,
  Tooltip,
  Avatar,
} from '@chakra-ui/react';
import { Dispatch, SetStateAction, useRef, useState } from 'react';
import {
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  getSortedRowModel,
  SortingState,
  useReactTable,
} from '@tanstack/react-table';
import { BsFillCaretUpFill, BsCaretDownFill } from 'react-icons/bs';
import { HiLockClosed, HiLockOpen } from 'react-icons/hi';
import Card from '@/components/card/Card';
import { User } from '@/utils/interface';
import useAuthAxios from '@/hooks/useAuthAxios';
import { CONFIRM } from '@/utils/constants/message';
import { toLocalDateTime } from '@/utils/datetime';

const columnHelper = createColumnHelper<User>();

export default function CheckTable(props: {
  data: User[];
  setData: Dispatch<SetStateAction<User[]>>;
}) {
  const { data, setData } = props;
  const [sorting, setSorting] = useState<SortingState>([]);
  const textColor = useColorModeValue('secondaryGray.900', 'white');
  const borderColor = useColorModeValue('gray.200', 'whiteAlpha.100');
  const [selectedUser, setSelectedUser] = useState(-1);
  const { getAPI } = useAuthAxios();
  const toast = useToast();
  const lockModal = useDisclosure();
  const unlockModal = useDisclosure();
  const lockRef = useRef(null);
  const unlockRef = useRef(null);

  const columns = [
    columnHelper.accessor('avatar_img_url', {
      id: 'avatar',
      header: () => <></>,
      cell: (info) => <Avatar src={info.getValue()} />,
    }),
    columnHelper.accessor('username', {
      id: 'username',
      header: () => (
        <Text
          justifyContent="space-between"
          align="center"
          fontSize={{ sm: '10px', lg: '12px' }}
          color="gray.400"
        >
          USERNAME
        </Text>
      ),
      cell: (info) => (
        <Flex align="center">
          <Text color={textColor} fontSize="sm" fontWeight="700">
            {info.getValue()}
          </Text>
        </Flex>
      ),
    }),
    columnHelper.accessor('email', {
      id: 'email',
      header: () => (
        <Text
          justifyContent="space-between"
          align="center"
          fontSize={{ sm: '10px', lg: '12px' }}
          color="gray.400"
        >
          EMAIL
        </Text>
      ),
      cell: (info) => (
        <Text color={textColor} fontSize="sm" fontWeight="700" maxWidth="200px">
          {info.getValue()}
        </Text>
      ),
    }),
    columnHelper.accessor('created_at', {
      id: 'created_at',
      header: () => (
        <Text
          justifyContent="space-between"
          align="center"
          fontSize={{ sm: '10px', lg: '12px' }}
          color="gray.400"
        >
          CREATED AT
        </Text>
      ),
      cell: (info) => (
        <Text color={textColor} fontSize="sm" fontWeight="700">
          {toLocalDateTime(info.getValue())}
        </Text>
      ),
    }),
    columnHelper.accessor('status', {
      id: 'status',
      header: () => (
        <Text
          justifyContent="space-between"
          align="center"
          fontSize={{ sm: '10px', lg: '12px' }}
          color="gray.400"
        >
          STATUS
        </Text>
      ),
      cell: (info) => (
        <Text color={textColor} fontSize="sm" fontWeight="700">
          {info.getValue()}
        </Text>
      ),
    }),
    columnHelper.accessor('code', {
      id: 'code',
      header: () => <></>,
      cell: (info) =>
        info.row.getValue('status') === 'ACTIVE' ? (
          <Tooltip label="Lock">
            <Button
              colorScheme="red"
              w="40px"
              h="40px"
              p="0"
              onClick={() => {
                setSelectedUser(info.row.index);
                lockModal.onOpen();
              }}
            >
              <HiLockClosed />
            </Button>
          </Tooltip>
        ) : (
          <Tooltip label="Unlock">
            <Button
              colorScheme="green"
              w="40px"
              h="40px"
              p="0"
              onClick={() => {
                setSelectedUser(info.row.index);
                unlockModal.onOpen();
              }}
            >
              <HiLockOpen />
            </Button>
          </Tooltip>
        ),
    }),
  ];

  const table = useReactTable({
    data,
    columns,
    state: {
      sorting,
    },
    onSortingChange: setSorting,
    getCoreRowModel: getCoreRowModel(),
    getSortedRowModel: getSortedRowModel(),
  });

  const handleCancel = () => {
    setSelectedUser(-1);
    lockModal.onClose();
    unlockModal.onClose();
  };

  const handleLock = async () => {
    const res = await getAPI(`/user/${data[selectedUser].username}/lock`);
    if (res.code === 200) {
      toast({
        description: 'Account is locked.',
        status: 'success',
        position: 'top-right',
      });
      setData((prev) => {
        const res = [...prev];
        res[selectedUser].status = 'INACTIVE';
        return res;
      });
    } else {
      toast({
        description: res.data as string,
        status: 'error',
        position: 'top-right',
      });
    }
    setSelectedUser(-1);
    lockModal.onClose();
  };

  const handleUnlock = async () => {
    const res = await getAPI(`/user/${data[selectedUser].username}/unlock`);
    if (res.code === 200) {
      toast({
        description: 'Account is unlocked.',
        status: 'success',
        position: 'top-right',
      });
      setData((prev) => {
        const res = [...prev];
        res[selectedUser].status = 'ACTIVE';
        return res;
      });
    } else {
      toast({
        description: res.data as string,
        status: 'error',
        position: 'top-right',
      });
    }
    setSelectedUser(-1);
    unlockModal.onClose();
  };

  return (
    <Card flexDirection="column" w="100%" px="0px" overflowX="auto">
      <Box>
        <Table variant="simple" color="gray.500" mt="12px">
          <Thead>
            {table.getHeaderGroups().map((headerGroup) => (
              <Tr key={headerGroup.id}>
                {headerGroup.headers.map((header) => {
                  return (
                    <Th
                      key={header.id}
                      colSpan={header.colSpan}
                      pe="10px"
                      borderColor={borderColor}
                      cursor="pointer"
                      onClick={header.column.getToggleSortingHandler()}
                    >
                      <Flex
                        justifyContent="space-between"
                        align="center"
                        fontSize={{ sm: '10px', lg: '12px' }}
                        color="gray.400"
                      >
                        {flexRender(
                          header.column.columnDef.header,
                          header.getContext()
                        )}
                        {{
                          asc: <BsFillCaretUpFill />,
                          desc: <BsCaretDownFill />,
                        }[header.column.getIsSorted() as string] ?? null}
                      </Flex>
                    </Th>
                  );
                })}
              </Tr>
            ))}
          </Thead>
          <Tbody>
            {table
              .getRowModel()
              .rows.slice(0, 11)
              .map((row) => {
                return (
                  <Tr key={row.id}>
                    {row.getVisibleCells().map((cell) => {
                      return (
                        <Td
                          key={cell.id}
                          fontSize={{ sm: '14px' }}
                          minW={{ sm: '150px', md: '200px', lg: 'auto' }}
                          borderColor="transparent"
                        >
                          {flexRender(
                            cell.column.columnDef.cell,
                            cell.getContext()
                          )}
                        </Td>
                      );
                    })}
                  </Tr>
                );
              })}
          </Tbody>
        </Table>
      </Box>
      <AlertDialog
        isOpen={lockModal.isOpen}
        leastDestructiveRef={lockRef}
        onClose={lockModal.onClose}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Lock Account
            </AlertDialogHeader>

            <AlertDialogBody>{CONFIRM}</AlertDialogBody>

            <AlertDialogFooter>
              <Button ref={lockRef} onClick={handleCancel}>
                Cancel
              </Button>
              <Button colorScheme="red" onClick={handleLock} ml={3}>
                Lock
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
      <AlertDialog
        isOpen={unlockModal.isOpen}
        leastDestructiveRef={unlockRef}
        onClose={unlockModal.onClose}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Unlock Account
            </AlertDialogHeader>

            <AlertDialogBody>{CONFIRM}</AlertDialogBody>

            <AlertDialogFooter>
              <Button ref={lockRef} onClick={handleCancel}>
                Cancel
              </Button>
              <Button colorScheme="green" onClick={handleUnlock} ml={3}>
                Unlock
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </Card>
  );
}

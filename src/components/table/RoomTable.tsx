import {
  Flex,
  Box,
  Table,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useColorModeValue,
  Button,
  AlertDialog,
  AlertDialogOverlay,
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogBody,
  AlertDialogFooter,
  useDisclosure,
  useToast,
  Tooltip,
} from '@chakra-ui/react';
import { Dispatch, SetStateAction, useRef, useState } from 'react';
import {
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  getSortedRowModel,
  SortingState,
  useReactTable,
} from '@tanstack/react-table';
import { BsFillCaretUpFill, BsCaretDownFill } from 'react-icons/bs';
import { HiLockClosed, HiLockOpen } from 'react-icons/hi';
import Card from '@/components/card/Card';
import { Room } from '@/utils/interface';
import MyBadge from '../Badge';
import useAuthAxios from '@/hooks/useAuthAxios';
import { CONFIRM } from '@/utils/constants/message';
import { toLocalDateTime } from '@/utils/datetime';

const columnHelper = createColumnHelper<Room>();

export default function CheckTable(props: {
  data: Room[];
  setData: Dispatch<SetStateAction<Room[]>>;
}) {
  const { data, setData } = props;
  const [sorting, setSorting] = useState<SortingState>([]);
  const textColor = useColorModeValue('secondaryGray.900', 'white');
  const borderColor = useColorModeValue('gray.200', 'whiteAlpha.100');
  const [selectedRoom, setSelectedRoom] = useState(-1);
  const { getAPI } = useAuthAxios();
  const toast = useToast();
  const lockModal = useDisclosure();
  const unlockModal = useDisclosure();
  const lockRef = useRef(null);
  const unlockRef = useRef(null);

  const columns = [
    columnHelper.accessor('name', {
      id: 'name',
      header: () => (
        <Text
          justifyContent="space-between"
          align="center"
          fontSize={{ sm: '10px', lg: '12px' }}
          color="gray.400"
        >
          NAME
        </Text>
      ),
      cell: (info) => (
        <Flex align="center">
          <Text color={textColor} fontSize="sm" fontWeight="700">
            {info.getValue()}
          </Text>
        </Flex>
      ),
    }),
    columnHelper.accessor('description', {
      id: 'description',
      header: () => (
        <Text
          justifyContent="space-between"
          align="center"
          fontSize={{ sm: '10px', lg: '12px' }}
          color="gray.400"
        >
          DESCRIPTION
        </Text>
      ),
      cell: (info) => (
        <Text color={textColor} fontSize="sm" fontWeight="700" maxWidth="200px">
          {info.getValue()?.substring(0, 255)}
        </Text>
      ),
    }),
    columnHelper.accessor('topics', {
      id: 'topics',
      header: () => (
        <Text
          justifyContent="space-between"
          align="center"
          fontSize={{ sm: '10px', lg: '12px' }}
          color="gray.400"
        >
          TOPIC
        </Text>
      ),
      cell: (info) => (
        <Flex flexWrap="wrap" gap="10px" maxWidth="200px">
          {info.getValue()?.map((topic) => (
            <MyBadge
              key={topic.name}
              color={topic.color}
              whiteSpace="normal"
              maxW="100%"
            >
              {topic.name}
            </MyBadge>
          ))}
        </Flex>
      ),
    }),
    columnHelper.accessor('room_type', {
      id: 'room_type',
      header: () => (
        <Text
          justifyContent="space-between"
          align="center"
          fontSize={{ sm: '10px', lg: '12px' }}
          color="gray.400"
        >
          TYPE
        </Text>
      ),
      cell: (info) => (
        <Text color={textColor} fontSize="sm" fontWeight="700">
          {info.getValue()}
        </Text>
      ),
    }),
    columnHelper.accessor('created_at', {
      id: 'created_at',
      header: () => (
        <Text
          justifyContent="space-between"
          align="center"
          fontSize={{ sm: '10px', lg: '12px' }}
          color="gray.400"
        >
          CREATED AT
        </Text>
      ),
      cell: (info) => (
        <Text color={textColor} fontSize="sm" fontWeight="700">
          {toLocalDateTime(info.getValue())}
        </Text>
      ),
    }),
    columnHelper.accessor('created_by', {
      id: 'created_by',
      header: () => (
        <Text
          justifyContent="space-between"
          align="center"
          fontSize={{ sm: '10px', lg: '12px' }}
          color="gray.400"
        >
          OWNER
        </Text>
      ),
      cell: (info) => (
        <Text color={textColor} fontSize="sm" fontWeight="700">
          {info.getValue()}
        </Text>
      ),
    }),
    columnHelper.accessor('status', {
      id: 'status',
      header: () => (
        <Text
          justifyContent="space-between"
          align="center"
          fontSize={{ sm: '10px', lg: '12px' }}
          color="gray.400"
        >
          STATUS
        </Text>
      ),
      cell: (info) => (
        <Text color={textColor} fontSize="sm" fontWeight="700">
          {info.getValue()}
        </Text>
      ),
    }),
    columnHelper.accessor('code', {
      id: 'code',
      header: () => <></>,
      cell: (info) =>
        info.row.getValue('status') === 'ACTIVE' ? (
          <Tooltip label="Lock">
            <Button
              colorScheme="red"
              w="40px"
              h="40px"
              p="0"
              onClick={() => {
                setSelectedRoom(info.row.index);
                lockModal.onOpen();
              }}
            >
              <HiLockClosed />
            </Button>
          </Tooltip>
        ) : (
          <Tooltip label="Unlock">
            <Button
              colorScheme="green"
              w="40px"
              h="40px"
              p="0"
              onClick={() => {
                setSelectedRoom(info.row.index);
                unlockModal.onOpen();
              }}
            >
              <HiLockOpen />
            </Button>
          </Tooltip>
        ),
    }),
  ];

  const table = useReactTable({
    data,
    columns,
    state: {
      sorting,
    },
    onSortingChange: setSorting,
    getCoreRowModel: getCoreRowModel(),
    getSortedRowModel: getSortedRowModel(),
  });

  const handleCancel = () => {
    setSelectedRoom(-1);
    lockModal.onClose();
    unlockModal.onClose();
  };

  const handleLock = async () => {
    const res = await getAPI(`/room/${data[selectedRoom].code}/lock`);
    if (res.code === 200) {
      toast({
        description: 'Room is locked.',
        status: 'success',
        position: 'top-right',
      });
      setData((prev) => {
        const res = [...prev];
        res[selectedRoom].status = 'INACTIVE';
        return res;
      });
    } else {
      toast({
        description: res.data as string,
        status: 'error',
        position: 'top-right',
      });
    }
    setSelectedRoom(-1);
    lockModal.onClose();
  };

  const handleUnlock = async () => {
    const res = await getAPI(`/room/${data[selectedRoom].code}/unlock`);
    if (res.code === 200) {
      toast({
        description: 'Room is unlocked.',
        status: 'success',
        position: 'top-right',
      });
      setData((prev) => {
        const res = [...prev];
        res[selectedRoom].status = 'ACTIVE';
        return res;
      });
    } else {
      toast({
        description: res.data as string,
        status: 'error',
        position: 'top-right',
      });
    }
    setSelectedRoom(-1);
    unlockModal.onClose();
  };

  return (
    <Card flexDirection="column" w="100%" px="0px" overflowX="auto">
      <Box>
        <Table variant="simple" color="gray.500" mt="12px">
          <Thead>
            {table.getHeaderGroups().map((headerGroup) => (
              <Tr key={headerGroup.id}>
                {headerGroup.headers.map((header) => {
                  return (
                    <Th
                      key={header.id}
                      colSpan={header.colSpan}
                      pe="10px"
                      borderColor={borderColor}
                      cursor="pointer"
                      onClick={header.column.getToggleSortingHandler()}
                    >
                      <Flex
                        justifyContent="space-between"
                        align="center"
                        fontSize={{ sm: '10px', lg: '12px' }}
                        color="gray.400"
                      >
                        {flexRender(
                          header.column.columnDef.header,
                          header.getContext()
                        )}
                        {{
                          asc: <BsFillCaretUpFill />,
                          desc: <BsCaretDownFill />,
                        }[header.column.getIsSorted() as string] ?? null}
                      </Flex>
                    </Th>
                  );
                })}
              </Tr>
            ))}
          </Thead>
          <Tbody>
            {table
              .getRowModel()
              .rows.slice(0, 11)
              .map((row) => {
                return (
                  <Tr key={row.id}>
                    {row.getVisibleCells().map((cell) => {
                      return (
                        <Td
                          key={cell.id}
                          fontSize={{ sm: '14px' }}
                          minW={{ sm: '150px', md: '200px', lg: 'auto' }}
                          borderColor="transparent"
                        >
                          {flexRender(
                            cell.column.columnDef.cell,
                            cell.getContext()
                          )}
                        </Td>
                      );
                    })}
                  </Tr>
                );
              })}
          </Tbody>
        </Table>
      </Box>
      <AlertDialog
        isOpen={lockModal.isOpen}
        leastDestructiveRef={lockRef}
        onClose={lockModal.onClose}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Lock Room
            </AlertDialogHeader>

            <AlertDialogBody>{CONFIRM}</AlertDialogBody>

            <AlertDialogFooter>
              <Button ref={lockRef} onClick={handleCancel}>
                Cancel
              </Button>
              <Button colorScheme="red" onClick={handleLock} ml={3}>
                Lock
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
      <AlertDialog
        isOpen={unlockModal.isOpen}
        leastDestructiveRef={unlockRef}
        onClose={unlockModal.onClose}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Unlock Room
            </AlertDialogHeader>

            <AlertDialogBody>{CONFIRM}</AlertDialogBody>

            <AlertDialogFooter>
              <Button ref={lockRef} onClick={handleCancel}>
                Cancel
              </Button>
              <Button colorScheme="green" onClick={handleUnlock} ml={3}>
                Unlock
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </Card>
  );
}
